from django.db import models
import datetime

# Create your models here.



class Post(models.Model):
    text = models.TextField()

    def __str__(self):
        return self.text[:50]


class Question(models.Model):
    question_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')

    def __str__(self):
        return self.question_text
    def was_published_recently(self):
        return self.pub_date >= timezone.now() - datetime.timedelta(days=1)

    def get_fields(self):
        return [(field.name, field.value_to_string(self)) for field in Question._meta.fields]

class Choice(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)

    def __str__(self):
        return self.choice_text

class Contact(models.Model):
    name = models.CharField(max_length=255)
    email = models.CharField(max_length=255)
    phone = models.CharField(max_length=50)
    created = models.DateTimeField(default=datetime.datetime.now)

    def __str__(self):
        return self.name
