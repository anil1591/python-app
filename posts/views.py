from django.shortcuts import get_object_or_404,render
from django.views.generic import ListView
from .models import Post
from .models import Question
from .models import Contact
from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader
from django.urls import reverse
# Create your views here.

class HomePageView(ListView):
    model = Post
    template_name = 'home.html'
    context_object_name = 'all_posts_list'

    def polls(request):
        latest_question_list = Question.objects.order_by('-pub_date')[:5]
        #template = loader.get_template('polls/index.html')
        context = {
            'latest_question_list': latest_question_list,
        }
        #return HttpResponse(template.render(context, request))
        return render(request, 'polls/index.html', context)

    def detail(request, question_id):
        try:
            #question = Question.objects.get(pk=question_id)
            question = get_object_or_404(Question, pk=question_id)

            context = {
                    'question': question,
            }

        except Question.DoesNotExist:
            raise Http404("Question Does Not Exist")
        return render(request, 'polls/detail.html', context)

    def results(request, question_id):
        question = get_object_or_404(Question, pk=question_id)
        return render(request, 'polls/results.html', {'question': question})

    def vote(request, question_id):
        question = get_object_or_404(Question, pk=question_id)
        try:
            selected_choice = question.choice_set.get(pk=request.POST['choice'])
        except (KeyError, Choice.DoesNotExist):
            # Redisplay the question voting form.
            return render(request, 'polls/detail.html', {
                'question': question,
                'error_message': "You didn't select a choice.",
            })
        else:
            selected_choice.votes += 1
            selected_choice.save()
            # Always return an HttpResponseRedirect after successfully dealing
            # with POST data. This prevents data from being posted twice if a
            # user hits the Back button.
        return HttpResponseRedirect(reverse('results', args=(question.id,)))

    def conatct_form(request):
        return render(request, 'contact/form.html')

    def contact_submit(request):
        contact =  Contact()
        contact.name = request.POST['name']
        contact.email = request.POST['email']
        contact.phone = request.POST['phone']
        contact.save()
        return HttpResponseRedirect(reverse('allcontacts', ))

    def conatct_detail(request):
            try:
                contact = Contact.objects.all()
                #contact = get_object_or_404(Contact)

                context = {
                        'contacts': contact,
                }

            except Contact.DoesNotExist:
                raise Http404("Contact Does Not Exist")
            #return render(request, 'polls/detail.html', context)
            else:
                print("fine")
            return render(request, 'contact/contacts.html',{'contacts':contact})
    def contact_delete(request,contact_id):
        contact = Contact.objects.get(pk=contact_id)
        contact.delete()
        return HttpResponseRedirect(reverse('allcontacts', ))

    def contact_edit(request,contact_id):
        try:
            contact = Contact.objects.get(pk=contact_id)
            if request.POST['submit']:
                contact_id  = request.POST['id']
                contact = Contact.objects.get(pk=contact_id)
                contact.name = request.POST['name']
                contact.email = request.POST['email']
                contact.phone = request.POST['phone']
                contact.save()
                return HttpResponseRedirect(reverse('allcontacts', ))

            return render(request, 'contact/edit.html', {'contact': contact})

        except (KeyError, Contact.DoesNotExist):
           return render(request, 'contact/edit.html', {
               'contact': contact,
               'error_message': "User does not exist.",
           })
        else:
            return HttpResponseRedirect(reverse('allcontacts', ))
    def contact_profile(request,contact_id):
        contact = Contact.objects.get(pk=contact_id)
        return render(request, 'contact/profile.html', {'contact': contact})
