"""mb_project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,include
from django.views.generic.base import TemplateView
from posts.views import HomePageView

from django.conf import settings
from django.conf.urls.static import static

app_name = 'poll'
urlpatterns = [
    path('admin/', admin.site.urls),

    path('', HomePageView.polls, name='poll'),
    path('polls/<int:question_id>/', HomePageView.detail, name='detail'),
    # ex: /polls/5/results/
    path('polls/<int:question_id>/results/', HomePageView.results, name='results'),
    # ex: /polls/5/vote/
    path('polls/<int:question_id>/vote/', HomePageView.vote, name='vote'),
    path('contact/', HomePageView.conatct_form, name='contact'),
        path('polls/contacts/', HomePageView.contact_submit, name='contacts'),
    path('allcontacts/', HomePageView.conatct_detail, name='allcontacts'),
    path('contact/<int:contact_id>', HomePageView.contact_delete, name='contact_delete'),
    path('contact/edit/<int:contact_id>', HomePageView.contact_edit, name='contact_edit'),
    path('contact/profile/<int:contact_id>', HomePageView.contact_profile, name='profile'),



] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
